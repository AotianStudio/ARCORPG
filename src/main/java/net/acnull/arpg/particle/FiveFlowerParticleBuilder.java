package net.acnull.arpg.particle;

import com.destroystokyo.paper.ParticleBuilder;
import org.bukkit.Location;

public class FiveFlowerParticleBuilder extends RedStoneParticleBuilder{

    @Override
    public ParticleBuilder spawn() {
        Location loc=location().clone();
        for (int d=0;d<360; d++) {
            double radians = Math.toRadians(d);
            double x = Math.cos(radians)*(Math.sin(radians*5)*radius()+radius());
            double z = Math.sin(radians)*(Math.sin(radians*5)*radius()+radius());
            loc.getWorld().spawnParticle(particle(),loc.clone().add(x,0,z),0,offsetX(),offsetY(),offsetZ(),extra(),data());
        }
        return this;
    }
}
