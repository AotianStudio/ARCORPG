package net.acnull.arpg.particle;

import com.destroystokyo.paper.ParticleBuilder;
import org.bukkit.Location;
import org.bukkit.util.Vector;

public class SwastikaParticle extends RedStoneParticleBuilder{
    @Override
    public ParticleBuilder spawn() {
        Location loc=location().clone();
        double deltaX=Math.cos(Math.toRadians(loc.getYaw()));
        double deltaZ=Math.sin(Math.toRadians(loc.getYaw()));
        Location[] locations =new Location[]{
                loc.clone().add(radius()*deltaX,radius(),radius()*deltaZ),
                loc.clone().add(0,radius(),0),
                loc.clone().add(-radius()*deltaX,radius(),-radius()*deltaZ),
                loc.clone().add(radius()*deltaX,0,radius()*deltaZ),
                loc.clone().add(-radius()*deltaX,0,-radius()*deltaZ),
                loc.clone().add(radius()*deltaX,-radius(),radius()*deltaZ),
                loc.clone().add(0,-radius(),0),
                loc.clone().add(-radius()*deltaX,-radius(),-radius()*deltaZ)
        };
        line(locations[0],locations[1],0.05f);
        line(locations[1],locations[6],0.05f);
        line(locations[6],locations[7],0.05f);
        line(locations[3],locations[5],0.05f);
        line(locations[3],locations[4],0.05f);
        line(locations[4],locations[2],0.05f);
        return this;
    }

    private void line(Location loc1,Location loc2,float delta){
        loc1=loc1.clone();
        loc2=loc2.clone();
        Vector vector=loc2.subtract(loc1).toVector();
        double length=vector.length();
        vector.normalize();
        for(float i=0;i<length;i+=delta) {
            Vector vector1=vector.clone().multiply(i);
            loc1.add(vector1);
            loc1.getWorld().spawnParticle(particle(),loc1,0,offsetX(),offsetY(),offsetZ(),extra(),data());
            loc1.subtract(vector1);
        }
    }
}
