package net.acnull.arpg.particle;

import com.destroystokyo.paper.ParticleBuilder;
import org.bukkit.Location;
import org.bukkit.Particle;

import java.util.Random;

public class PlaneParticleBuilder extends ParticleBuilder {

    private int radius=1;
    private int density=10;
    private ParticleBuilder son;

    public PlaneParticleBuilder(Particle particle){
        super(particle);
        son = new ParticleBuilder(particle).count(0);
    }
    public PlaneParticleBuilder(ParticleBuilder builder){
        super(builder.particle());
        son = builder;
    }

    @Override
    public ParticleBuilder spawn() {
        Location loc;
        Random random=new Random();
        for(int f=0;f<density;f++){
            loc=location().clone();
            double x = 1-random.nextDouble()*2;
            double y = Math.sin(Math.acos(x));
            int func =( random.nextDouble()>=0.5? 1: -1);
            double v =random.nextDouble();
            loc.add(x*v*radius,0,y*v*radius*func);
            son.location(loc).spawn();
        }
        return this;
    }


    public PlaneParticleBuilder radius(int radius){
        this.radius=radius;
        return this;
    }

    public PlaneParticleBuilder density(int density){
        this.density=density;
        return this;
    }

}
