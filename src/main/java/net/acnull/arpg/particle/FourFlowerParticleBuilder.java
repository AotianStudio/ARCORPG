package net.acnull.arpg.particle;

import com.destroystokyo.paper.ParticleBuilder;
import org.bukkit.Location;

public class FourFlowerParticleBuilder extends RedStoneParticleBuilder{
    @Override
    public ParticleBuilder spawn() {
        Location loc=location().clone();
        double deltaX=Math.cos(Math.toRadians(loc.getYaw()));
        double deltaZ=Math.sin(Math.toRadians(loc.getYaw()));
        for (int d=0;d<360; d++) {
            double radians = Math.toRadians(d);
            double x = Math.cos(radians)*(Math.sin(radians*4)*radius()+1*radius());
            double y = Math.sin(radians)*(Math.sin(radians*4)*radius()+1*radius());
            loc.getWorld().spawnParticle(particle(),loc.clone().add(x*deltaX,y,x*deltaZ),0,offsetX(),offsetY(),offsetZ(),extra(),data());
        }
        return this;
    }
}
