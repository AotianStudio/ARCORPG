package net.acnull.arpg.particle;

import com.destroystokyo.paper.ParticleBuilder;
import org.bukkit.Location;
import org.bukkit.Particle;

import java.util.Random;

public class RedStoneParticleBuilder extends ParticleBuilder{

    private float radius=0.5f;

    public RedStoneParticleBuilder(){
        super(Particle.REDSTONE);
    }

    @Override
    public ParticleBuilder spawn() {
        Location loc=location().clone();
        float r=radius*10;
        Random random=new Random();
        for (int i = 0; i < count(); i++) {
            loc.add((random.nextInt((int)(r*2))-r)*0.1,
                    (random.nextInt((int)(r*2))-r)*0.1,
                    (random.nextInt((int)(r*2))-r)*0.1);
            loc.getWorld().spawnParticle(particle(),loc,0,offsetX(),offsetY(),offsetZ(),extra(),data());
        }
        return this;
    }

    public RedStoneParticleBuilder radius(float radius){
        this.radius=radius;
        return this;
    }

    public float radius(){
        return this.radius;
    }

}
