package net.acnull.arpg.particle;

import com.destroystokyo.paper.ParticleBuilder;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.World;
import org.bukkit.util.Vector;

import java.util.Random;


public class ChopParticleBuilder extends ParticleBuilder {
    private double length;
    private Vector vector;
    public ChopParticleBuilder(Particle particle,double _length) {
        super(particle);
        length= _length;
    }
    public ChopParticleBuilder(Particle particle,double _length,Vector _vector) {
        super(particle);
        length= _length;
        vector=_vector;
    }
    @Override
    public ParticleBuilder spawn() {
        if(vector==null) {
            vector = Vector.getRandom();
            Random random = new Random();
            vector.setX(vector.getX() * (random.nextDouble() >= 0.5 ? -1 : 1));
            vector.setY(vector.getY() * (random.nextDouble() >= 0.5 ? -1 : 1));
            vector.setZ(vector.getZ() * (random.nextDouble() >= 0.5 ? -1 : 1));
        }
        vector.normalize();
        vector.multiply(length/2);
        Location start = location().clone().add(vector);
        vector.normalize();
        vector.multiply(-0.2);
        World world = start.getWorld();
        for(double f = 0;f<length;f+=0.2){
            world.spawnParticle(particle(),start,0,offsetX(),offsetY(),offsetZ(),extra(),data());
            start.add(vector);
        }
        return this;
    }
}
