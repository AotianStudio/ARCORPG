package net.acnull.arpg.commands;

import net.acnull.arpg.effect.effects.monster.CircleLineFallAttack;
import net.acnull.arpg.particle.FiveFlowerParticleBuilder;
import net.acnull.arpg.particle.RedStoneParticleBuilder;
import net.acnull.arpg.rpg.AbstractRPG;
import net.acnull.arpg.rpg.RPGManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ARPGCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if(!(sender instanceof Player)||!sender.isOp()){
            sender.sendMessage("You can not do this!");
            return true;
        }
        if(args.length==0){
            sender.sendMessage("Bad arguments.");
            new CircleLineFallAttack(((Player) sender).getPlayer()
                    , 1,10,50,8
                    ,new RedStoneParticleBuilder().radius(0.05f).color(255,204,255).count(1)
                    ,new FiveFlowerParticleBuilder().radius(1f).color(255,204,255).count(5)).run();
            /*
            Zombie zombie=((Player) sender).getWorld().spawn(((Player) sender).getLocation(),Zombie.class);
            AIManager manager=new AIManager(zombie);
            /*
            manager.addAI(new LineAttackAIItem(zombie,new ParticleBuilder(Particle.FLAME).count(0),1,30,15,1),3);
            manager.addAI(new TeleportAIItem(zombie,15),2);
            zombie.getAttribute(Attribute.GENERIC_FOLLOW_RANGE).setBaseValue(30);
            manager.addAI(new AOEAttackAIItem(zombie),2);*/
            /*
            ArmorStand stand=((Player) sender).getWorld().spawn(((Player) sender).getLocation(),ArmorStand.class);
            stand.setCanPickupItems(false);
            stand.setInvulnerable(true);
            stand.setMarker(true);
            stand.setItemInHand(new ItemStack(Material.DIAMOND_SWORD));
            stand.setVisible(false);
            stand.setGravity(false);
            stand.setRightArmPose(new EulerAngle(120,0,0));
            return true;
             */
            return true;
        }
        AbstractRPG rpg= RPGManager.getInstance().getRPGByName(args[0]);
        if(rpg==null){
            sender.sendMessage("Item not found.");
            return true;
        }
        ((Player) sender).getInventory().addItem(rpg.getItem());
        return true;
    }
}
