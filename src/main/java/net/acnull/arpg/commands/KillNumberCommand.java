package net.acnull.arpg.commands;

import net.acnull.arpg.kill.KillNumberHelper;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class KillNumberCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if(!(sender instanceof Player)||!sender.isOp()){
            sender.sendMessage("You can not do this!");
            return true;
        }
        Player p=(Player)sender;
        if(args.length<2){
            p.sendMessage("Bad Arguments.");
            return true;
        }
        ItemStack item=p.getEquipment().getItemInMainHand();
        if(!item.hasItemMeta()||!item.getItemMeta().hasLore()){
            return true;
        }
        KillNumberHelper helper=null;
        ItemMeta meta=item.getItemMeta();
        List<String> list=new ArrayList<>(meta.getLore());
        int i;
        for(i=0;i<meta.getLore().size();i++) {
            String lore=item.getItemMeta().getLore().get(i);
            helper = KillNumberHelper.parseString(lore);
            if (helper == null) {
                continue;
            }
            if(helper.getLevel()+1>helper.getMaxLevel()){
                break;
            }
            helper.setLevel(helper.getLevel()+1);
            break;
        }
        if (args[0].equalsIgnoreCase("set")){
            if(helper==null){
                sender.sendMessage("Kill Number Not Found.");
                return true;
            }
            helper.setLevel(Integer.parseInt(args[1]));
            list.set(i,helper.toString());
        }
        if (args[0].equalsIgnoreCase("setMax")){
            if(helper==null){
                helper=KillNumberHelper.createHelper(Integer.parseInt(args[1]));
                list.add(helper.toString());
            }else {
                helper.setMaxLevel(Integer.parseInt(args[1]));
                list.set(i, helper.toString());
            }
        }
        meta.setLore(list);
        item.setItemMeta(meta);
        p.sendMessage("successfully.");
        return true;
    }
}
