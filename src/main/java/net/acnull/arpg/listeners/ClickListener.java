package net.acnull.arpg.listeners;

import com.destroystokyo.paper.ParticleBuilder;
import net.acnull.arpg.effect.Light;
import net.acnull.arpg.effect.effects.monster.DelayAOEAttack;
import net.acnull.arpg.particle.PlaneParticleBuilder;
import net.acnull.arpg.particle.RedStoneParticleBuilder;
import net.acnull.arpg.rpg.AbstractRPG;
import net.acnull.arpg.rpg.RPGManager;
import net.acnull.arpg.rpg.skill.Skill;
import net.acnull.arpg.rpg.skill.Skillable;
import org.bukkit.Color;
import org.bukkit.Particle;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class ClickListener implements Listener {

    @EventHandler
    public void onClick(PlayerInteractEvent event){
        ItemStack item=event.getPlayer().getEquipment().getItemInMainHand();
        if(item!=null&&item.hasItemMeta()&&item.getItemMeta().hasLore()){
            AbstractRPG rpg=RPGManager.getInstance().getRPGByFirstLore(item.getItemMeta().getLore().get(0));
            if(rpg instanceof Skillable){
                Skill skill=((Skillable)rpg).getSkill();
                skill.onClick(event.getPlayer(),event.getAction());
            }
        }
        /*
        Location loc=event.getPlayer().getLocation();
        double deltaX=Math.cos(Math.toRadians(loc.getYaw()+90));
        double deltaZ=Math.sin(Math.toRadians(loc.getYaw()+90));
        for (int i = 0; i < 5; i++) {
            loc.add(2*deltaX,0,2*deltaZ);
            new StarFall(loc.clone(),event.getPlayer()).run();
        }*/
    }

    @EventHandler
    public void test(AsyncPlayerChatEvent event){
        if(event.getMessage().equals("test")){
            new DelayAOEAttack(event.getPlayer(),40,3,30,new ParticleBuilder(Particle.REDSTONE).count(0).color(Color.SILVER),new PlaneParticleBuilder(Particle.SMOKE_NORMAL).radius(1).density(20),new PlaneParticleBuilder(Particle.EXPLOSION_NORMAL).radius(3).density(200)).run();
            new Light(event.getPlayer().getLocation(),40,8).create();
            event.getPlayer().sendMessage("test");
        }
    }
}
