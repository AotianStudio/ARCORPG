package net.acnull.arpg.listeners;

import net.acnull.arpg.rpg.AbstractRPG;
import net.acnull.arpg.rpg.RPGManager;
import net.acnull.arpg.rpg.Sword;
import net.acnull.arpg.rpg.skill.IHurt;
import net.acnull.arpg.rpg.skill.Skill;
import net.acnull.arpg.rpg.skill.Skillable;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.ItemStack;

public class AttackListener implements Listener {
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onAttack(EntityDamageByEntityEvent event){
        if(!(event.getDamager() instanceof LivingEntity)||!(event.getEntity() instanceof LivingEntity)){
            return;
        }
        if(event.getCause()== EntityDamageEvent.DamageCause.CUSTOM){
            return;
        }
        LivingEntity attacker=(LivingEntity)event.getDamager();
        LivingEntity entity=(LivingEntity)event.getEntity();
        ItemStack item=attacker.getEquipment().getItemInMainHand();
        if(item!=null&&item.hasItemMeta()&&item.getItemMeta().hasLore()){
            AbstractRPG rpg= RPGManager.getInstance().getRPGByFirstLore(item.getItemMeta().getLore().get(0));
            if(rpg instanceof Skillable){
                Skill skill=((Skillable)rpg).getSkill();
                skill.onDamage(entity);
                double f=event.getDamage()/attacker.getAttribute(Attribute.GENERIC_ATTACK_DAMAGE).getValue();
                event.setDamage(f* ((Sword) rpg).getDamage());
            }
        }
    }
}
