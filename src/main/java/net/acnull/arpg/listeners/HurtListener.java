package net.acnull.arpg.listeners;

import net.acnull.arpg.rpg.AbstractRPG;
import net.acnull.arpg.rpg.RPGManager;
import net.acnull.arpg.rpg.skill.IHurt;
import net.acnull.arpg.rpg.skill.Skill;
import net.acnull.arpg.rpg.skill.Skillable;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.ItemStack;

public class HurtListener implements Listener {
    @EventHandler
    public void onHurt(EntityDamageEvent event){
        if(!(event.getEntity() instanceof LivingEntity)){
            return;
        }
        ItemStack item=((LivingEntity)event.getEntity()).getEquipment().getItemInMainHand();
        if(item!=null&&item.hasItemMeta()&&item.getItemMeta().hasLore()){
            AbstractRPG rpg= RPGManager.getInstance().getRPGByFirstLore(item.getItemMeta().getLore().get(0));
            if(rpg instanceof Skillable){
                Skill skill=((Skillable)rpg).getSkill();
                if(skill instanceof IHurt){
                    ((IHurt) skill).onHurt(event);
                }
            }
        }
    }
}
