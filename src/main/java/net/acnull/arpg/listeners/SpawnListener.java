package net.acnull.arpg.listeners;

import com.zeus.pathfinder.AI.AIItem.AbstractAIItem;
import com.zeus.pathfinder.Manager.AIManager;
import io.lumine.xikage.mythicmobs.api.bukkit.events.MythicMobSpawnEvent;
import net.acnull.arpg.monster.CooldownAIManager;
import net.acnull.arpg.monster.IMonster;
import net.acnull.arpg.monster.MonsterManager;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class SpawnListener implements Listener {
    @EventHandler
    public void onSpawn(MythicMobSpawnEvent event){
        if(!(event.getEntity() instanceof Monster)){
            return;
        }
        Monster monster=(Monster)event.getEntity();
        IMonster monster1=MonsterManager.getInstance().getMonsterByName(event.getMobType().getDisplayName());
        if(monster1!=null) {
            CooldownAIManager manager = new CooldownAIManager((LivingEntity)event.getEntity(),20);
            for(AbstractAIItem item: monster1.getAIItems(monster)){
                manager.addAI(item,2);
            }
            if(monster1.isNoSprint()){
                monster.getAttribute(Attribute.GENERIC_KNOCKBACK_RESISTANCE).setBaseValue(20f);
            }
        }

    }
}
