package net.acnull.arpg.effect;

import org.bukkit.Location;

public abstract class Effect{

    private final Location location;

    public Effect(Location location){
        this.location=location.clone();
    }

    public Location getLocation() {
        return location;
    }
}
