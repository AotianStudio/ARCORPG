package net.acnull.arpg.effect;

import net.acnull.arpg.Main;
import org.bukkit.Bukkit;
import ru.beykerykt.lightapi.LightAPI;
import ru.beykerykt.lightapi.LightType;
import ru.beykerykt.lightapi.chunks.ChunkInfo;

import java.util.ArrayList;
import java.util.List;

class LightManager {

    private static final LightManager manager=new LightManager();

    private final List<Light> lights;

    public static LightManager getInstance(){
        return manager;
    }

    private LightManager(){
        lights=new ArrayList<>();
        Bukkit.getScheduler().runTaskTimer(Main.getPlugin(Main.class),()->{
            List<Light> removes=new ArrayList<>();
            for(Light light:lights){
                light.setTime(light.getTime()-1);
                if(light.getTime()==0){
                    removes.add(light);
                }
            }
            for(Light light:removes){
                lights.remove(light);
                LightAPI.deleteLight(light.getLocation(), LightType.BLOCK,false);
                for(ChunkInfo info:LightAPI.collectChunks(light.getLocation(),LightType.BLOCK,light.getLevel())) {
                    LightAPI.updateChunk(info, LightType.BLOCK);
                }
            }
        },20,20);
    }

    public void addLight(Light light){
        lights.add(light);
        LightAPI.createLight(light.getLocation(), LightType.BLOCK,light.getLevel(),false);
        for(ChunkInfo info:LightAPI.collectChunks(light.getLocation(),LightType.BLOCK,light.getLevel())) {
            LightAPI.updateChunk(info, LightType.BLOCK);
        }
    }

}
