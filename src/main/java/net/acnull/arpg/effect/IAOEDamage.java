package net.acnull.arpg.effect;

public interface IAOEDamage extends IDamage {
    int getRadius();
    boolean isCall();
}
