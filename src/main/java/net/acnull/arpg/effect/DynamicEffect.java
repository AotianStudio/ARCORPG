package net.acnull.arpg.effect;

import org.bukkit.Location;

public abstract class DynamicEffect extends Effect{

    public DynamicEffect(Location location) {
        super(location);
    }

    public void run(){
        DynamicEffectManager.getInstance().addEffect(this);
    }

    public abstract int getRange();
    public abstract boolean next();

}
