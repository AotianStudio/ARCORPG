package net.acnull.arpg.effect;

import org.bukkit.Location;

public class Light{

    private Location location;
    private int time;
    private int level;

    public Light(Location location,int time,int level){
        this.location=location.clone();
        this.time=time;
        this.level=level;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getLevel() {
        return level;
    }

    public Location getLocation() {
        return location;
    }

    public void create(){
        LightManager.getInstance().addLight(this);
    }

}
