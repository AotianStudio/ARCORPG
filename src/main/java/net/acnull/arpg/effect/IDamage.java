package net.acnull.arpg.effect;

import org.bukkit.Bukkit;
import org.bukkit.EntityEffect;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

public interface IDamage {
    LivingEntity getOwner();
    void onDamage(LivingEntity entity);
    default void payHurt(LivingEntity entity,float damage){
        EntityDamageByEntityEvent event=new EntityDamageByEntityEvent(getOwner(),entity, EntityDamageEvent.DamageCause.CUSTOM,damage);
        Bukkit.getPluginManager().callEvent(event);
        if(event.isCancelled()){
            return;
        }
        double delta=entity.getHealth()- event.getFinalDamage();
        if(delta<=0){
            entity.setHealth(0);
        }else{
            entity.setHealth(delta);
            entity.playEffect(EntityEffect.HURT);
        }
    }
}
