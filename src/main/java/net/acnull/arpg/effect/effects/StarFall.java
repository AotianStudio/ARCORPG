package net.acnull.arpg.effect.effects;

import net.acnull.arpg.effect.DynamicEffect;
import net.acnull.arpg.effect.IDamage;
import net.acnull.arpg.effect.Light;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.LivingEntity;

public class StarFall extends DynamicEffect implements IDamage {

    private final LivingEntity owner;
    private boolean hasAttack;

    public StarFall(Location location,LivingEntity owner) {
        super(location);
        this.owner=owner;
        getLocation().add(0,10,0);
    }

    @Override
    public int getRange() {
        return 1;
    }

    @Override
    public void run() {
        super.run();
        new Light(getLocation().clone().subtract(0,10,0),4,15).create();
    }

    @Override
    public boolean next(){
        Location loc=getLocation();
        loc.getWorld().spawnParticle(Particle.PORTAL,loc.subtract(0,0.1,0),0);
        loc.getWorld().spawnParticle(Particle.FLAME,loc.subtract(0,0.1,0),0);
        loc.getWorld().spawnParticle(Particle.LAVA,loc.subtract(0,0.1,0),0);
        loc.getWorld().spawnParticle(Particle.REDSTONE,loc.subtract(0,0.1,0),0);
        loc.getWorld().spawnParticle(Particle.CLOUD,loc.subtract(0,0.1,0),0);
        return !loc.getBlock().getType().isSolid()&&!hasAttack;
    }

    @Override
    public LivingEntity getOwner() {
        return owner;
    }

    @Override
    public void onDamage(LivingEntity entity) {
        payHurt(entity,4);
        hasAttack=true;
    }
}
