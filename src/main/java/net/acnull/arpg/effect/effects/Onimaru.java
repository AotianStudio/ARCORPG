package net.acnull.arpg.effect.effects;

import com.destroystokyo.paper.ParticleBuilder;
import net.acnull.arpg.Main;
import net.acnull.arpg.effect.DynamicEffect;
import net.acnull.arpg.effect.IDamage;
import net.acnull.arpg.effect.Light;
import net.acnull.arpg.particle.RedStoneParticleBuilder;
import net.acnull.arpg.particle.SwastikaParticle;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.LivingEntity;

public class Onimaru extends DynamicEffect implements IDamage {

    private final LivingEntity owner;
    private final ParticleBuilder builder=new RedStoneParticleBuilder().radius(0.1f).count(10).color(0x11,0xee,0x11);
    private final ParticleBuilder body=new SwastikaParticle().radius(1f).color(0x11,0xee,0x11);
    private final double deltaX;
    private final double deltaY;
    private final double deltaZ;

    private double timer;
    private boolean hasAttack;

    public Onimaru(Location location,LivingEntity owner) {
        super(location);
        this.owner=owner;
        this.deltaX=Math.cos(Math.toRadians(location.getYaw()+90));
        this.deltaY=Math.cos(Math.toRadians(location.getPitch()+90));
        this.deltaZ=Math.sin(Math.toRadians(location.getYaw()+90));
    }

    @Override
    public int getRange() {
        return 2;
    }

    @Override
    public boolean next() {
        timer+=1;
        Location loc=getLocation().add(deltaX,deltaY,deltaZ);
        new Light(loc,1,15).create();
        builder.location(loc).spawn();
        if(loc.getBlock().getType().isSolid()||timer>15){
            getLocation().subtract(deltaX, deltaY, deltaZ);
            body.location(getLocation()).spawn();
            loc.getWorld().playSound(loc,Sound.ENTITY_BLAZE_SHOOT,5,20);
            return false;
        }
        return !hasAttack;
    }

    @Override
    public LivingEntity getOwner() {
        return owner;
    }

    @Override
    public void onDamage(LivingEntity entity) {
        body.location(getLocation()).spawn();
        hasAttack=true;
        entity.setAI(false);
        entity.getWorld().playSound(entity.getLocation(),Sound.ENTITY_BLAZE_SHOOT,10,10);
        Bukkit.getScheduler().runTaskLater(Main.getPlugin(Main.class),()->{
            entity.setAI(true);
        },60);
    }
}
