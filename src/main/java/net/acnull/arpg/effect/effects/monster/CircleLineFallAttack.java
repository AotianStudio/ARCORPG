package net.acnull.arpg.effect.effects.monster;

import com.destroystokyo.paper.ParticleBuilder;
import net.acnull.arpg.effect.DynamicEffect;
import net.acnull.arpg.effect.effects.LineFall;
import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;

import java.util.Random;

public class CircleLineFallAttack extends DynamicEffect {

    private final LivingEntity entity;
    private final int count;
    private final int radius;
    private final ParticleBuilder line;
    private final ParticleBuilder body;
    private final float damage;
    private final int last;

    private final Random ran;

    private int timer;

    public CircleLineFallAttack(LivingEntity entity, float damage, int last, int count, int radius, ParticleBuilder line, ParticleBuilder body) {
        super(entity.getLocation());
        this.entity=entity;
        this.count=count;
        this.radius=radius;
        this.line=line;
        this.body=body;
        this.damage=damage;
        this.last=last*10;
        ran=new Random();
    }

    @Override
    public int getRange() {
        return 2;
    }

    @Override
    public boolean next() {
        if(ran.nextInt(100)<count){
            int x=ran.nextInt(2*radius)-radius;
            int z=ran.nextInt(2*radius)-radius;
            Location loc=entity.getLocation().add(x,0,z);
            new LineFall(loc.clone(),damage,entity,line,body).run();
        }
        return timer++<last;
    }
}
