package net.acnull.arpg.effect.effects.monster;

import net.acnull.arpg.effect.DynamicEffect;
import net.acnull.arpg.effect.IAOEDamage;
import net.acnull.arpg.particle.ChopParticleBuilder;
import net.acnull.arpg.particle.PlaneParticleBuilder;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.LivingEntity;

public class FlashChopAttack extends DynamicEffect implements IAOEDamage {
    private int tick;
    private int last;
    private int radius;
    private int damage;
    private int range;
    private LivingEntity owner;

    public FlashChopAttack(Location location,int _last,int _radius,int _damage,int _range,LivingEntity _owner) {
        super(location);
        tick = 0;
        last=_last;
        radius=_radius;
        range = _range;
        owner=_owner;
        damage=_damage;
    }

    @Override
    public int getRange() {
        return range;
    }

    @Override
    public boolean next() {
        if(tick<=last){
            new PlaneParticleBuilder(new ChopParticleBuilder(Particle.REDSTONE,radius*2).count(0).color(Color.SILVER))
                    .radius(radius/2)
                    .density(3)
                    .location(getLocation())
                    .spawn();
            tick+=range;
            return true;
        }else return false;
    }

    @Override
    public int getRadius() {
        return radius;
    }

    @Override
    public boolean isCall() {
        return true;
    }

    @Override
    public LivingEntity getOwner() {
        return owner;
    }

    @Override
    public void onDamage(LivingEntity entity) {
        payHurt(entity,damage);
    }
}
