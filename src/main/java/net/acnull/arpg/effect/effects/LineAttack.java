package net.acnull.arpg.effect.effects;

import com.destroystokyo.paper.ParticleBuilder;
import net.acnull.arpg.effect.DynamicEffect;
import net.acnull.arpg.effect.IDamage;
import net.acnull.arpg.effect.Light;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.Vector;

import java.util.function.Supplier;

public class LineAttack extends DynamicEffect implements IDamage {

    private final LivingEntity owner;
    private final ParticleBuilder builder;
    private final int length;
    private final float damage;
    private final int level;
    private double deltaX;
    private double deltaY;
    private double deltaZ;
    private Vector vector;

    private final Supplier<Location> supplier;

    private double timer;
    private boolean hasAttack;

    private ParticleBuilder body;
    private Sound blockSound;
    private Sound entitySound;

    public LineAttack(Location location,LivingEntity owner, ParticleBuilder builder, int length, float damage, int level) {
        super(location);
        this.owner=owner;
        this.builder=builder;
        this.length=length;
        this.damage=damage;
        this.level=level;
        this.deltaX=Math.cos(Math.toRadians(location.getYaw()+90));
        this.deltaY=Math.cos(Math.toRadians(location.getPitch()+90));
        this.deltaZ=Math.sin(Math.toRadians(location.getYaw()+90));
        supplier=()-> getLocation().add(deltaX,deltaY,deltaZ);
    }
    public LineAttack(Location location,Location target,LivingEntity owner, ParticleBuilder builder, int length, float damage, int level) {
        super(location);
        this.owner=owner;
        this.builder=builder;
        this.length=length;
        this.damage=damage;
        this.level=level;
        vector=target.subtract(location).toVector().normalize();
        supplier=()->getLocation().add(vector.clone().multiply(1));
    }

    @Override
    public int getRange() {
        return 1;
    }

    @Override
    public boolean next() {
        timer+=1;
        Location loc=supplier.get();
        new Light(loc,1,level).create();
        builder.location(loc).spawn();
        if(loc.getBlock().getType().isSolid()){
            if(blockSound!=null){
                loc.getWorld().playSound(loc,blockSound,10,10);
            }
            if(body!=null){
                body.location(getLocation()).spawn();
            }
            return false;
        }
        return timer<length&&!hasAttack;
    }

    @Override
    public LivingEntity getOwner() {
        return owner;
    }

    @Override
    public void onDamage(LivingEntity entity) {
        if(entitySound!=null){
            entity.getWorld().playSound(entity.getLocation(),entitySound,10,10);
        }
        if(body!=null){
            body.location(getLocation()).spawn();
        }
        payHurt(entity,damage);
        hasAttack=true;
    }

    public void setBlockSound(Sound blockSound) {
        this.blockSound = blockSound;
    }

    public void setEntitySound(Sound entitySound) {
        this.entitySound = entitySound;
    }

    public void setBody(ParticleBuilder body) {
        this.body = body;
    }
}
