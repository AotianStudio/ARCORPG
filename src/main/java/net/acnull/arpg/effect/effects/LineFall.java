package net.acnull.arpg.effect.effects;

import com.destroystokyo.paper.ParticleBuilder;
import net.acnull.arpg.effect.DynamicEffect;
import net.acnull.arpg.effect.IDamage;
import net.acnull.arpg.effect.Light;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.LivingEntity;

public class LineFall extends DynamicEffect implements IDamage {

    private final LivingEntity owner;
    private final ParticleBuilder line;
    private final ParticleBuilder body;
    private final float damage;

    private boolean hasAttack;

    public LineFall(Location location,float damage, LivingEntity owner,ParticleBuilder line,ParticleBuilder body) {
        super(location);
        this.owner=owner;
        getLocation().add(0,8,0);
        this.line=line;
        this.body=body;
        this.damage=damage;
    }

    @Override
    public int getRange() {
        return 2;
    }

    @Override
    public void run() {
        super.run();
        new Light(getLocation().clone().subtract(0,8,0),4,15).create();
    }

    @Override
    public boolean next(){
        Location loc=getLocation();
        for(int i=0;i<3;i++){
            loc.subtract(0,0.1,0);
            line.location(loc).spawn();
        }
        if(loc.getBlock().getType().isSolid()||hasAttack){
            body.location(loc.add(0,0.5,0)).spawn();
            return false;
        }
        return true;
    }

    @Override
    public LivingEntity getOwner() {
        return owner;
    }

    @Override
    public void onDamage(LivingEntity entity) {
        payHurt(entity,damage);
        hasAttack=true;
    }
}
