package net.acnull.arpg.effect.effects.monster;

import com.destroystokyo.paper.ParticleBuilder;
import net.acnull.arpg.effect.DynamicEffect;
import net.acnull.arpg.effect.IAOEDamage;
import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;

public class DelayAOEAttack extends DynamicEffect implements IAOEDamage {
    private ParticleBuilder bound;
    private ParticleBuilder body;
    private ParticleBuilder filter;
    private int delay;
    private int radius;
    private int tick;
    private int damage;
    private LivingEntity entity;
    private LivingEntity owner;
    private boolean iscall;

    public DelayAOEAttack(Location location, LivingEntity _owner, int _delay, int _radius, int _damage, ParticleBuilder _bound, ParticleBuilder _body, ParticleBuilder _filter) {
        super(location);
        delay=_delay;
        bound=_bound;
        body=_body;
        filter=_filter;
        radius=_radius;
        tick=0;
        iscall=false;
        damage=_damage;
        owner = _owner;
    }

    public DelayAOEAttack(LivingEntity _entity, int _delay,int _radius, int _damage, ParticleBuilder _bound, ParticleBuilder _body, ParticleBuilder _filter) {
        super(_entity.getLocation());
        delay=_delay;
        bound=_bound;
        body=_body;
        filter=_filter;
        radius=_radius;
        tick=0;
        iscall=false;
        damage=_damage;
        entity=_entity;
        owner=_entity;
    }

    @Override
    public int getRange() {
        return 1;
    }

    @Override
    public boolean next() {
        if(tick==0){
            for (int degree = 0; degree < 360 * (radius / 5 + 1); degree++) {
                double radians = Math.toRadians(degree);
                double x = Math.cos(radians);
                double y = Math.sin(radians);
                bound.location(getLocation().clone().add(x * radius, 0, y * radius)).spawn();
            }
        }
        if(tick<=delay) {
            if(tick%7==0) {
                if(entity!=null){
                    Location location = entity.getLocation();
                    getLocation().setX(location.getX());
                    getLocation().setY(location.getY());
                    getLocation().setZ(location.getZ());
                    getLocation().setWorld(location.getWorld());
                }
                body.location(getLocation()).spawn();
            }
            if(tick==delay) iscall=true;
            tick++;
            return true;
        }else {
            filter.location(getLocation()).spawn();
            return false;
        }
    }

    @Override
    public LivingEntity getOwner() {
        return owner;
    }

    @Override
    public void onDamage(LivingEntity entity) {
        payHurt(entity, damage);
    }

    @Override
    public boolean isCall() {
        return iscall;
    }

    @Override
    public int getRadius() {
        return radius;
    }
}
