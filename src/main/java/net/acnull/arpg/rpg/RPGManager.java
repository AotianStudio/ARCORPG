package net.acnull.arpg.rpg;

import java.util.ArrayList;
import java.util.List;

public class RPGManager {
    private static final RPGManager manager=new RPGManager();

    private final List<AbstractRPG> rpgs;

    private RPGManager(){
        rpgs=new ArrayList<>();
    }

    public void addRPG(AbstractRPG rpg){
        rpgs.add(rpg);
    }

    public AbstractRPG getRPGByName(String name){
        return rpgs.stream().filter(rpg->rpg.getName().equalsIgnoreCase(name)).findFirst().orElse(null);
    }

    public AbstractRPG getRPGByFirstLore(String lore){
        return rpgs.stream()
                .filter(rpg->rpg.getItem().getItemMeta().getLore().get(0).equals(lore))
                .findFirst().orElse(null);
    }

    public static RPGManager getInstance(){
        return manager;
    }
}
