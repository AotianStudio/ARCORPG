package net.acnull.arpg.rpg.skill.skills;

import com.destroystokyo.paper.ParticleBuilder;
import net.acnull.arpg.cooldown.CooldownManager;
import net.acnull.arpg.effect.effects.Onimaru;
import net.acnull.arpg.particle.RedStoneParticleBuilder;
import net.acnull.arpg.rpg.skill.IDeath;
import net.acnull.arpg.rpg.skill.IHurt;
import net.acnull.arpg.rpg.skill.Skill;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageEvent;

import java.util.Map;
import java.util.UUID;

public class OnimaruSkill implements Skill,IDeath, IHurt {

    private final ParticleBuilder builder=new RedStoneParticleBuilder().color(0x11,0xee,0x11).count(30);
    private final ParticleBuilder death=new RedStoneParticleBuilder().color(Color.PURPLE).count(30);

    @Override
    public void onClick(Player player,Action action) {
        if(action!=Action.RIGHT_CLICK_AIR&&action!=Action.RIGHT_CLICK_BLOCK){
            return;
        }
        Map<UUID,Integer> map= CooldownManager.getInstance().getMap("onimaru");
        if(map.containsKey(player.getUniqueId())){
            return;
        }
        new Onimaru(player.getLocation().add(0,player.getEyeHeight(),0),player).run();
        map.put(player.getUniqueId(),8);
    }

    @Override
    public void onDamage(LivingEntity entity) {
        builder.location(entity.getEyeLocation()).spawn();
        entity.setFireTicks(60);
    }

    @Override
    public void onDeath(LivingEntity entity) {
        Player p=entity.getKiller();
        double health=p.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue();
        double health1=p.getHealth()+0.1*health;
        p.setHealth(Math.min(health, health1));
        death.location(p.getEyeLocation()).spawn();
        if(entity.getLocation().subtract(0,1,0).getBlock().getType().isSolid()&&entity.getLocation().getBlock().getType()==Material.AIR){
            entity.getLocation().getBlock().setType(Material.FIRE);
        }
    }

    @Override
    public void onHurt(EntityDamageEvent event) {
        if(event.getCause()== EntityDamageEvent.DamageCause.FIRE||
                event.getCause()== EntityDamageEvent.DamageCause.FIRE_TICK){
            event.setCancelled(true);
            event.getEntity().setFireTicks(0);
        }
    }
}
