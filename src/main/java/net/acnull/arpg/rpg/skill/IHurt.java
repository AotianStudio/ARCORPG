package net.acnull.arpg.rpg.skill;

import org.bukkit.event.entity.EntityDamageEvent;

public interface IHurt {
    void onHurt(EntityDamageEvent event);
}
