package net.acnull.arpg.rpg.skill.skills;

import net.acnull.arpg.cooldown.CooldownManager;
import net.acnull.arpg.effect.effects.StarFall;
import net.acnull.arpg.rpg.skill.Skill;
import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;

import java.util.Map;
import java.util.UUID;

public class StarFallSkill implements Skill {
    @Override
    public void onDamage(LivingEntity entity) {}

    @Override
    public void onClick(Player player,Action action){
        if(action!=Action.LEFT_CLICK_AIR&&action!=Action.LEFT_CLICK_BLOCK
                &&action!=Action.RIGHT_CLICK_AIR&&action!=Action.RIGHT_CLICK_BLOCK){
            return;
        }
        Map<UUID,Integer> map= CooldownManager.getInstance().getMap("starFall");
        if(map.containsKey(player.getUniqueId())){
            return;
        }
        Location loc=player.getEyeLocation();
        loc.setY(player.getLocation().getY());
        double deltaX=Math.cos(Math.toRadians(loc.getYaw()+90));
        double deltaZ=Math.sin(Math.toRadians(loc.getYaw()+90));
        for (int i = 0; i < 5; i++) {
            loc.add(2*deltaX,0,2*deltaZ);
            new StarFall(loc.clone(),player).run();
        }
        map.put(player.getUniqueId(),2);
    }
}
