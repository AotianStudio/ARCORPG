package net.acnull.arpg.rpg.skill;

import org.bukkit.entity.LivingEntity;

public interface IDeath {
    void onDeath(LivingEntity entity);
}
