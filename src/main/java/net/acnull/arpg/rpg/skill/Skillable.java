package net.acnull.arpg.rpg.skill;

public interface Skillable{
    Skill getSkill();
}
