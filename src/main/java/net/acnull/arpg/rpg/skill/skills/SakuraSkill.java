package net.acnull.arpg.rpg.skill.skills;

import com.destroystokyo.paper.ParticleBuilder;
import net.acnull.arpg.cooldown.CooldownManager;
import net.acnull.arpg.effect.IDamage;
import net.acnull.arpg.effect.Light;
import net.acnull.arpg.effect.effects.LineAttack;
import net.acnull.arpg.particle.FiveFlowerParticleBuilder;
import net.acnull.arpg.particle.FourFlowerParticleBuilder;
import net.acnull.arpg.rpg.skill.Skill;
import org.bukkit.Bukkit;
import org.bukkit.EntityEffect;
import org.bukkit.Sound;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

import java.util.Map;
import java.util.UUID;

public class SakuraSkill implements Skill {

    private final ParticleBuilder builder;
    private final int length;
    private final float damage;
    private final int level;
    private final int cooldown;
    private final int aoecooldown;

    private Sound entitySound;
    private Sound blockSound;

    private final ParticleBuilder body=new FiveFlowerParticleBuilder().radius(2.5f).color(255,204,255);

    public SakuraSkill(ParticleBuilder builder, int length, float damage, int level, int cooldown,int aoecooldown){
        this.builder=builder;
        this.length=length;
        this.damage=damage;
        this.level=level;
        this.cooldown=cooldown;
        this.aoecooldown=aoecooldown;
    }

    @Override
    public void onClick(Player player,Action action) {
        if(action==Action.LEFT_CLICK_AIR||action==Action.LEFT_CLICK_BLOCK){
            Map<UUID,Integer> map= CooldownManager.getInstance().getMap("sakura");
            if(map.containsKey(player.getUniqueId())){
                return;
            }
            if(cooldown>0) {
                map.put(player.getUniqueId(), cooldown);
            }
            LineAttack attack=new LineAttack(player.getEyeLocation().subtract(0,0.3,0)
                    ,player,builder,length,damage,level);
            attack.setBlockSound(blockSound);
            attack.setEntitySound(entitySound);
            attack.setBody(new FourFlowerParticleBuilder().radius(1f).color(255,204,255).count(5));
            attack.run();
        }else if(action==Action.RIGHT_CLICK_AIR||action==Action.RIGHT_CLICK_BLOCK){
            Map<UUID,Integer> map= CooldownManager.getInstance().getMap("sakura1");
            if(map.containsKey(player.getUniqueId())){
                return;
            }
            if(cooldown>0) {
                map.put(player.getUniqueId(), aoecooldown);
            }
            new Light(player.getLocation(),2,15).create();
            body.location(player.getLocation()).spawn();
            player.getLocation().getWorld()
                    .getNearbyEntities(player.getLocation(), 6, 0.2, 6).stream()
                    .filter(entity -> entity != player && entity instanceof LivingEntity)
                    .forEach(e->{
                        EntityDamageByEntityEvent event=new EntityDamageByEntityEvent(player,e, EntityDamageEvent.DamageCause.CUSTOM,damage);
                        Bukkit.getPluginManager().callEvent(event);
                        if(event.isCancelled()){
                            return;
                        }
                        ((LivingEntity) e).setHealth
                                (Math.max(((LivingEntity) e).getHealth()- event.getFinalDamage(),0));
                        e.playEffect(EntityEffect.HURT);
                    });
            player.getWorld().playSound(player.getLocation(),Sound.BLOCK_LAVA_POP,7,7);
        }
    }

    @Override
    public void onDamage(LivingEntity entity) {

    }

    public SakuraSkill setEntitySound(Sound entitySound) {
        this.entitySound = entitySound;
        return this;
    }

    public SakuraSkill setBlockSound(Sound blockSound) {
        this.blockSound = blockSound;
        return this;
    }
}
