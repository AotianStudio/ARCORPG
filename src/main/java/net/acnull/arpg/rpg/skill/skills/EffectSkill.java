package net.acnull.arpg.rpg.skill.skills;

import com.destroystokyo.paper.ParticleBuilder;
import net.acnull.arpg.effect.Light;
import net.acnull.arpg.effect.effects.StarFall;
import net.acnull.arpg.rpg.potion.Potion;
import net.acnull.arpg.rpg.potion.PotionBuilder;
import net.acnull.arpg.rpg.potion.PotionManager;
import net.acnull.arpg.rpg.skill.Skill;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EffectSkill implements Skill {

    private final ParticleBuilder particle;
    private final List<PotionBuilder> potions;
    private final int level;

    public EffectSkill(ParticleBuilder particle, int level, PotionBuilder... potions){
        this.particle=particle;
        this.level=level;
        this.potions=new ArrayList<>(Arrays.asList(potions));
    }

    @Override
    public void onDamage(LivingEntity entity) {
        if(particle!=null){
            particle.location(entity.getLocation()).spawn();
        }
        if(level!=0){
            new Light(entity.getLocation(),2,level).create();
        }
        if(potions!=null){
            for(PotionBuilder builder:potions){
                PotionManager.getInstance().addPotion(builder.createPotion(entity));
            }
        }
    }

    public List<PotionBuilder> getPotions() {
        return potions;
    }

    @Override
    public void onClick(Player player,Action action) {
        if(action!=Action.LEFT_CLICK_AIR&&action!=Action.LEFT_CLICK_BLOCK){
            return;
        }
        Location loc=player.getEyeLocation().subtract(0,1,0);
        double deltaX=Math.cos(Math.toRadians(loc.getYaw())+90);
        double deltaZ=Math.sin(Math.toRadians(loc.getYaw())+90);
        loc.add(deltaX,0,deltaZ);
        if(particle!=null){
            particle.location(loc.add(0,1,0)).spawn();
        }
        if(level!=0){
            new Light(loc,2,level).create();
        }
    }
}
