package net.acnull.arpg.rpg.skill;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;

public interface Skill {
    void onClick(Player player,Action action);
    void onDamage(LivingEntity entity);

}
