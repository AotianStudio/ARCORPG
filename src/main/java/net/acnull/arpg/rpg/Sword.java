package net.acnull.arpg.rpg;

import org.bukkit.inventory.ItemStack;

public abstract class Sword extends AbstractRPG{

    public abstract float getDamage();

    @Override
    public Type getType() {
        return Type.SWORD;
    }
}
