package net.acnull.arpg.rpg.swords;

import net.acnull.arpg.rpg.Sword;
import net.acnull.arpg.rpg.skill.Skill;
import net.acnull.arpg.rpg.skill.Skillable;

public class SkillSword extends Sword implements Skillable {

    private final Skill skill;

    private final float damage;
    private final String name;

    public SkillSword(Skill skill,float damage,String name) {
        this.skill = skill;
        this.damage=damage;
        this.name=name;
    }

    @Override
    public Skill getSkill() {
        return skill;
    }

    @Override
    public float getDamage() {
        return damage;
    }

    @Override
    public String getName() {
        return name;
    }


}
