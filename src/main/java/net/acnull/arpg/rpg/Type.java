package net.acnull.arpg.rpg;

public enum Type {
    SWORD,
    BOW,
    HELMET,
    CHESTPLATE,
    LEGGINGS,
    BOOTS;

    public boolean isArmor(Type type){
        return type==HELMET||
                type==CHESTPLATE||
                type==LEGGINGS||
                type==BOOTS;
    }
}
