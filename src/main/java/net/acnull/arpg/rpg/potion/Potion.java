package net.acnull.arpg.rpg.potion;

import org.bukkit.entity.LivingEntity;

public abstract class Potion{

    private LivingEntity entity;
    private int time;

    public Potion(LivingEntity entity,int time){
        this.entity=entity;
        this.time=time;
    }

    public LivingEntity getEntity() {
        return entity;
    }

    public int getTime(){
        return time;
    }

    public void setTime(int time){
        this.time=time;
    }

    public void start(){}
    public void next(){}
    public void end(){}
    public void onDie(){}
}
