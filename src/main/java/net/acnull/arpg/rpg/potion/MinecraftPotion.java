package net.acnull.arpg.rpg.potion;

import org.bukkit.entity.LivingEntity;
import org.bukkit.potion.PotionEffect;

public class MinecraftPotion extends Potion{

    private PotionEffect effect;

    public MinecraftPotion(LivingEntity entity,PotionEffect effect) {
        super(entity,effect.getDuration());
        this.effect=effect;
    }

    @Override
    public void start() {
        getEntity().addPotionEffect(effect);
    }
}
