package net.acnull.arpg.rpg.potion;

import org.bukkit.entity.LivingEntity;
import org.bukkit.potion.PotionEffect;

import java.lang.reflect.Constructor;

public class PotionBuilder {

    private Class<? extends Potion> clazz;
    private Object[] args;

    public PotionBuilder(Class<? extends Potion> clazz,Object... args){
        this.clazz=clazz;
        this.args=args;
    }

    public Potion createPotion(LivingEntity entity){
        Potion potion=null;
        if (MinecraftPotion.class.equals(clazz)) {
            potion=new MinecraftPotion(entity,(PotionEffect)args[0]);
        }else{
            try{
                Constructor<? extends Potion> constructor=clazz.getDeclaredConstructor(LivingEntity.class,int.class);
                constructor.setAccessible(true);
                potion=constructor.newInstance(entity,args[0]);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return potion;
    }

    public Class<? extends Potion> getClazz() {
        return clazz;
    }
}
