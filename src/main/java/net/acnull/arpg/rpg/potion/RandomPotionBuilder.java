package net.acnull.arpg.rpg.potion;

import org.bukkit.entity.LivingEntity;

import java.util.Random;

public class RandomPotionBuilder extends PotionBuilder{

    private final int t;

    public RandomPotionBuilder(Class<? extends Potion> clazz,int t, Object... args) {
        super(clazz, args);
        this.t=t;
    }

    @Override
    public Potion createPotion(LivingEntity entity) {
        if(new Random().nextInt(100)>t){
            return null;
        }
        return super.createPotion(entity);
    }
}
