package net.acnull.arpg.rpg.potion;

import net.acnull.arpg.Main;
import org.bukkit.Bukkit;
import org.bukkit.entity.LivingEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PotionManager {

    private static final PotionManager manager=new PotionManager();

    private final List<Potion> potions;

    private PotionManager(){
        potions=new ArrayList<>();
        Bukkit.getScheduler().runTaskTimer(Main.getPlugin(Main.class),()->{
            List<Potion> remove=new ArrayList<>();
            for(Potion potion:potions){
                potion.setTime(potion.getTime()-1);
                potion.next();
                if(potion.getTime()<=0){
                    remove.add(potion);
                    potion.end();
                }
            }
            for(Potion potion:remove){
                potions.remove(potion);
            }
        },20,20);
    }

    public void addPotion(Potion potion){
        if(potion==null){
            return;
        }
        potions.add(potion);
        potion.start();
    }

    public void removePotion(Potion potion){
        potions.remove(potion);
    }

    public List<Potion> getPotionsByEntity(LivingEntity entity){
        return potions.stream().filter(potion -> potion.getEntity()==entity).collect(Collectors.toList());
    }

    public static PotionManager getInstance() {
        return manager;
    }

}
