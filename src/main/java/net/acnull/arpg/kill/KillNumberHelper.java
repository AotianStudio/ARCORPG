package net.acnull.arpg.kill;

public class KillNumberHelper {

    private int maxLevel;
    private int level;

    private KillNumberHelper(){}

    public static KillNumberHelper parseString(String str){
        if(!str.startsWith("§a§b§c")){
            return null;
        }
        str=str.replaceFirst("§a§b§c","");
        StringBuilder maxLevel= new StringBuilder();
        StringBuilder level= new StringBuilder();
        for(int i=1;i<str.length();i+=2){
            char c=str.charAt(i);
            if(c=='c'){
                break;
            }
            maxLevel.append(c);
        }
        for(int i=str.length();i>0;i--){
            char c=str.charAt(i-1);
            if(!Character.isDigit(c)){
                break;
            }
            level.append(c);
        }
        KillNumberHelper helper=new KillNumberHelper();
        helper.maxLevel=Integer.parseInt(maxLevel.toString());
        helper.level=Integer.parseInt(level.reverse().toString());
        return helper;
    }

    public static KillNumberHelper createHelper(int maxLevel){
        KillNumberHelper helper=new KillNumberHelper();
        helper.maxLevel=maxLevel;
        return helper;
    }

    public int getLevel() {
        return level;
    }

    public int getMaxLevel() {
        return maxLevel;
    }

    public void setMaxLevel(int maxLevel) {
        this.maxLevel = maxLevel;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String toString(){
        StringBuilder lore= new StringBuilder("§a§b§c");
        String maxLevel=String.valueOf(this.maxLevel);
        for(char c:maxLevel.toCharArray()){
            lore.append("§").append(c);
        }
        lore.append("§c杀怪数量:").append(level);
        return lore.toString();
    }

}
