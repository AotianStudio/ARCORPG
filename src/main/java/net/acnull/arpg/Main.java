package net.acnull.arpg;

import net.acnull.arpg.commands.ARPGCommand;
import net.acnull.arpg.commands.KillNumberCommand;
import net.acnull.arpg.listeners.*;
import net.acnull.arpg.monster.MonsterManager;
import net.acnull.arpg.monster.monsters.Jiakang;
import net.acnull.arpg.monster.monsters.Xinchang;
import net.acnull.arpg.particle.RedStoneParticleBuilder;
import net.acnull.arpg.rpg.AbstractRPG;
import net.acnull.arpg.rpg.RPGManager;
import net.acnull.arpg.rpg.potion.ExplodingPotion;
import net.acnull.arpg.rpg.potion.MinecraftPotion;
import net.acnull.arpg.rpg.potion.RandomPotionBuilder;
import net.acnull.arpg.rpg.skill.skills.*;
import net.acnull.arpg.rpg.swords.SkillSword;
import org.bukkit.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.io.File;
import java.util.List;

public class Main extends JavaPlugin{

    private AbstractRPG[] rpgs;

    @Override
    public void onEnable() {
        getCommand("arpg").setExecutor(new ARPGCommand());
        getCommand("killnumber").setExecutor(new KillNumberCommand());
        if(!new File(getDataFolder(),"config.yml").exists()){
            saveDefaultConfig();
        }
        reloadConfig();
        loadItem();
        for(String key:getConfig().getKeys(false)){
            ConfigurationSection section=getConfig().getConfigurationSection(key);
            rpgs[Integer.parseInt(key)-1].setItem(
                    createItem(Material.getMaterial(section.getString("material"))
                            ,section.getString("name"),section.getStringList("lore")));
        }
        for(AbstractRPG rpg:rpgs){
            RPGManager.getInstance().addRPG(rpg);
        }
        MonsterManager.getInstance().addMonster(new Jiakang());
        MonsterManager.getInstance().addMonster(new Xinchang());
        Bukkit.getPluginManager().registerEvents(new ClickListener(),this);
        Bukkit.getPluginManager().registerEvents(new AttackListener(),this);
        Bukkit.getPluginManager().registerEvents(new DeathListener(),this);
        Bukkit.getPluginManager().registerEvents(new SpawnListener(),this);
        Bukkit.getPluginManager().registerEvents(new HurtListener(),this);
    }

    private static ItemStack createItem(Material material, String displayName, List<String> lore){
        ItemStack item=new ItemStack(material);
        ItemMeta meta=item.getItemMeta();
        meta.setDisplayName(displayName);
        meta.setLore(lore);
        meta.setUnbreakable(true);
        item.setItemMeta(meta);
        return item;
    }

    private void loadItem(){
        rpgs=new AbstractRPG[]{
                new SkillSword(new StarFallSkill(),9,"star"),
                new SkillSword(null,5,"cunzheng"),
                new SkillSword(new EffectSkill(
                        new RedStoneParticleBuilder().color(Color.RED).count(30),5
                        ,new RandomPotionBuilder(MinecraftPotion.class,30
                        ,new PotionEffect(PotionEffectType.SLOW,200,3)))
                        ,11,"xuecunzheng"),
                new SkillSword(null,5,"zhezong"),
                new SkillSword(null,5,"wumingdaocunzheng"),
                new SkillSword(new EffectSkill(new RedStoneParticleBuilder().color(Color.BLUE).count(30),8
                        ,new RandomPotionBuilder(MinecraftPotion.class,30,new PotionEffect(PotionEffectType.POISON,240,2))
                        ,new RandomPotionBuilder(MinecraftPotion.class,30,new PotionEffect(PotionEffectType.WITHER,240,2))),
                        16,"pingshenwu"),
                new SkillSword(new EffectSkill(null,5
                        ,new RandomPotionBuilder(MinecraftPotion.class,50,new PotionEffect(PotionEffectType.WEAKNESS,200,3))),
                        5, "wumingdaozhengzong"),
                new SkillSword(new CritSkill(4,25,9),21,"zhengzongwuren"),
                new SkillSword(
                        new EffectSkill(
                        new RedStoneParticleBuilder().color(Color.PURPLE).count(30),9
                        ,new RandomPotionBuilder(MinecraftPotion.class,30,new PotionEffect(PotionEffectType.POISON,200,1))
                        ,new RandomPotionBuilder(MinecraftPotion.class,30,new PotionEffect(PotionEffectType.BLINDNESS,200,1))
                        ,new RandomPotionBuilder(MinecraftPotion.class,30,new PotionEffect(PotionEffectType.WITHER,200,1))
                        ,new RandomPotionBuilder(MinecraftPotion.class,30,new PotionEffect(PotionEffectType.WEAKNESS,200,1))
                        ,new RandomPotionBuilder(MinecraftPotion.class,30,new PotionEffect(PotionEffectType.SLOW,200,1)))
                ,28,"yaodaocunzheng"),
                new SkillSword(
                        new OnimaruSkill(),
                        30,"guiwanguogang"),
                new SkillSword(new EffectSkill(null,9
                        ,new RandomPotionBuilder(ExplodingPotion.class,30,10))
                        ,27,"cunyu"),
                new SkillSword(new SakuraSkill(
                        new RedStoneParticleBuilder().radius(0.1f).color(255,204,255).count(10),
                        50,16,15,1,3
                ).setBlockSound(Sound.BLOCK_GRASS_BREAK)
                        .setEntitySound(Sound.BLOCK_STONE_HIT),30,"shanying"),
                new SkillSword(new SprintSkill(5,new RedStoneParticleBuilder()
                        .radius(0.5f).color(255,255,255).count(8),2,18)
                        ,18,"taidao")
        };
    }

}
