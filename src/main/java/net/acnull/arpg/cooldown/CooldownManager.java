package net.acnull.arpg.cooldown;

import net.acnull.arpg.Main;
import org.bukkit.Bukkit;

import java.util.*;

public class CooldownManager {

    private static final CooldownManager manager=new CooldownManager();
    private final Map<String,Map<UUID,Integer>> map;

    private CooldownManager(){
        map=new HashMap<>();
        Bukkit.getScheduler().runTaskTimer(Main.getPlugin(Main.class),()->{
            for(Map<UUID,Integer> map:this.map.values()){
                for (Map.Entry<UUID,Integer> entry:new ArrayList<>(map.entrySet())){
                    int i=entry.getValue()-1;
                    if(i!=0) {
                        map.put(entry.getKey(), i);
                        continue;
                    }
                    map.remove(entry.getKey());
                }
            }
        },20,20);
    }

    public Map<UUID,Integer> getMap(String key){
        return this.map.computeIfAbsent(key, k -> new HashMap<>());
    }

    public static CooldownManager getInstance() {
        return manager;
    }
}
