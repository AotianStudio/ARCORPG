package net.acnull.arpg.monster;

public interface ICooldown {
    int getCooldown();
    int getStrip();
    void setStrip(int strip);
    boolean isSuspended();
    void suspend(boolean b);
    default boolean isCooling(){
        if(isSuspended()) return true;
        return (getStrip()!=getCooldown());
    }
    default void readStrip(){
        if(isSuspended()) return;
        if(getStrip()==getCooldown()) return;
        setStrip(getStrip()+1);
    }
    default void reset(){
        setStrip(0);
    }
}
