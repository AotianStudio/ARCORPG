package net.acnull.arpg.monster;

import com.zeus.pathfinder.AI.AIItem.AbstractAIItem;
import org.bukkit.entity.Monster;

public interface IMonster {
    String getDisplayName();
    AbstractAIItem[] getAIItems(Monster monster);
    boolean isNoSprint();
}
