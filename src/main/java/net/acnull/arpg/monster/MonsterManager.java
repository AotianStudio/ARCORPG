package net.acnull.arpg.monster;

import java.util.ArrayList;
import java.util.List;

public class MonsterManager{

    private static final MonsterManager manager=new MonsterManager();

    private final List<IMonster> monsters;

    private MonsterManager(){
        monsters=new ArrayList<>();
    }

    public void addMonster(IMonster monster){
        this.monsters.add(monster);
    }

    public IMonster getMonsterByName(String name){
        return monsters.stream().filter(m->m.getDisplayName().equalsIgnoreCase(name))
                .findFirst().orElse(null);
    }


    public static MonsterManager getInstance() {
        return manager;
    }

}
