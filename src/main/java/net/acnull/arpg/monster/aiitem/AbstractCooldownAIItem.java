package net.acnull.arpg.monster.aiitem;

import com.zeus.pathfinder.AI.AIItem.AbstractAIItem;
import net.acnull.arpg.monster.CooldownAIManager;
import net.acnull.arpg.monster.ICooldown;

import java.util.UUID;

public abstract class AbstractCooldownAIItem extends AbstractAIItem implements ICooldown {
    private int cooldown;
    private int strip;
    private boolean suspend;
    private final UUID uuid;
    private CooldownAIManager manager;
    public AbstractCooldownAIItem(int _cooldown, CooldownAIManager _manager){
        cooldown=_cooldown;
        strip=0;
        suspend=false;
        uuid=UUID.randomUUID();
        manager=_manager;
    }
    public abstract int getDelay();
    public abstract boolean skill();
    @Override
    public boolean shouldExecute() {
        return true;
    }
    @Override
    public boolean continueExecute() {
        readStrip();
        if(isCooling()) return true;
        if (skill()) {
            manager.focus(this,getDelay());
            reset();
        }
        return true;
    }

    @Override
    public int getCooldown() {
        return cooldown;
    }

    @Override
    public int getStrip() {
        return strip;
    }

    @Override
    public void setStrip(int strip) {
        this.strip=strip;
    }

    @Override
    public boolean isSuspended() {
        return suspend;
    }

    @Override
    public void suspend(boolean b) {
        suspend=b;
    }

    public UUID getUuid() {
        return uuid;
    }

    public boolean equals(AbstractCooldownAIItem ai){
        return ai.uuid.equals(getUuid());
    }
}
