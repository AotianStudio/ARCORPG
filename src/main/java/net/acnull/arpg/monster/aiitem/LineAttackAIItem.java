package net.acnull.arpg.monster.aiitem;

import com.destroystokyo.paper.ParticleBuilder;
import com.zeus.pathfinder.AI.AIItem.AbstractAIItem;
import net.acnull.arpg.effect.effects.LineAttack;
import org.bukkit.entity.Monster;

import java.util.Random;

public class LineAttackAIItem extends AbstractAIItem {

    private final Monster monster;
    private final ParticleBuilder builder;
    private final float damage;
    private final int range;
    private final int level;
    private final int t;

    private final Random ran;


    public LineAttackAIItem(Monster monster, ParticleBuilder builder, float damage, int range, int level, int t){
        this.monster=monster;
        this.builder=builder;
        this.damage=damage;
        this.range=range;
        this.level=level;
        this.t=t;
        this.ran=new Random();
    }

    @Override
    public boolean shouldExecute() {
        return monster.getTarget()!=null&&!monster.getTarget().isDead();
    }

    @Override
    public boolean continueExecute() {
        if(monster.getTarget()==null||monster.getTarget().isDead()){
            return false;
        }
        if(ran.nextInt(100)<t){
            new LineAttack(monster.getEyeLocation(),monster.getTarget().getEyeLocation(),monster,builder,range,damage,level).run();
        }
        return true;
    }

}
