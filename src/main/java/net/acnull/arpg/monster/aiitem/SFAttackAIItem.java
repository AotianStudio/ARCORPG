package net.acnull.arpg.monster.aiitem;

import com.destroystokyo.paper.ParticleBuilder;
import com.zeus.pathfinder.AI.AIItem.AbstractAIItem;
import net.acnull.arpg.effect.effects.monster.CircleLineFallAttack;
import org.bukkit.entity.Monster;

import java.util.Random;

public class SFAttackAIItem extends AbstractAIItem{

    private final Monster monster;
    private final int t;
    private final ParticleBuilder line;
    private final ParticleBuilder body;
    private final float damage;
    private final int last;
    private final int count;
    private final int radius;

    private final Random ran;

    public SFAttackAIItem(Monster monster, int t,float damage,int last,int count,int radius, ParticleBuilder line,ParticleBuilder body){
        this.monster=monster;
        ran=new Random();
        this.t=t;
        this.line=line;
        this.body=body;
        this.damage=damage;
        this.last=last;
        this.count=count;
        this.radius=radius;
    }

    @Override
    public boolean shouldExecute() {
        return monster.getTarget()!=null&&!monster.getTarget().isDead()&&ran.nextInt(200)<t;
    }

    @Override
    public void startExecute() {
        new CircleLineFallAttack(monster,damage,last,count,radius,line,body).run();
    }

    @Override
    public boolean continueExecute() {
        return false;
    }
}
