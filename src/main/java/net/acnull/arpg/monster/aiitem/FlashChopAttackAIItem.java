package net.acnull.arpg.monster.aiitem;

import net.acnull.arpg.Main;
import net.acnull.arpg.effect.Light;
import net.acnull.arpg.effect.effects.monster.FlashChopAttack;
import net.acnull.arpg.monster.CooldownAIManager;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Monster;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;


public class FlashChopAttackAIItem extends AbstractCooldownAIItem{
    private Monster monster;
    private int delay;
    private int radius;
    private int damage;

    public FlashChopAttackAIItem(Monster monster, int _delay, int _radius, int _damage, int _cooldown, CooldownAIManager manager) {
        super(_cooldown,manager);
        this.monster = monster;
        delay=_delay;
        radius=_radius;
        damage=_damage;
    }

    @Override
    public int getDelay() {
        return delay;
    }

    @Override
    public boolean shouldExecute() {
        return monster.getTarget() != null && !monster.getTarget().isDead() ;
    }

    @Override
    public boolean skill() {
        if(monster ==null||monster.isDead()||monster.getTarget()==null||monster.getTarget().isDead())return false;
        Location loc = monster.getLocation().clone();
        if(loc.distance(monster.getTarget().getLocation())>radius) return false;
        loc.setY(loc.getY()+1);
        monster.setNoDamageTicks(delay+5);
        monster.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY,delay+5,1));
        monster.addPotionEffect(new PotionEffect(PotionEffectType.SLOW,delay+5,5));
        new BukkitRunnable(){
            @Override
            public void run() {
                monster.getLocation().getWorld().playSound(monster.getLocation(), Sound.ENTITY_PLAYER_ATTACK_STRONG,1,1);
                new Light(monster.getLocation(),2,12).create();
                new FlashChopAttack(loc,2,radius,damage,2,monster).run();
            }
        }.runTaskLater(Main.getPlugin(Main.class),delay);
        return true;
    }
}
