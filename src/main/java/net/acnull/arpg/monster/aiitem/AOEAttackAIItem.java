package net.acnull.arpg.monster.aiitem;

import com.destroystokyo.paper.ParticleBuilder;
import net.acnull.arpg.Main;
import net.acnull.arpg.effect.Light;
import net.acnull.arpg.effect.effects.monster.DelayAOEAttack;
import net.acnull.arpg.monster.CooldownAIManager;
import net.acnull.arpg.particle.PlaneParticleBuilder;
import org.bukkit.*;
import org.bukkit.entity.Monster;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;


public class AOEAttackAIItem extends AbstractCooldownAIItem {

    private final Monster monster;
    private int delay;
    private int radius;
    private int damage;
    private int distend;

    public AOEAttackAIItem(Monster monster, int _delay, int _radius, int _damage, int _distend, int _cooldown, CooldownAIManager manager) {
        super(_cooldown,manager);
        this.monster = monster;
        delay=_delay;
        radius=_radius;
        damage=_damage;
        distend=_distend;
    }

    @Override
    public int getDelay() {
        return delay;
    }

    @Override
    public boolean shouldExecute() {
        return monster.getTarget() != null && !monster.getTarget().isDead();
    }

    @Override
    public boolean skill() {
        if(monster ==null||monster.isDead()||monster.getTarget()==null||monster.getTarget().isDead())return false;
        Location loc = monster.getLocation().clone();
        Vector vector = loc.clone().subtract(monster.getTarget().getLocation()).toVector();
        double length = vector.length();
        vector.normalize();
        if(distend<length)vector.multiply(-distend);
        else vector.multiply(-length);
        loc.add(vector);
        double alpha =loc.getWorld().getHighestBlockAt(loc).getLocation().getY();
        if(Math.abs(alpha-monster.getLocation().getY())>2) return false;
        loc.setY(alpha);
        monster.setNoDamageTicks(delay+20);
        monster.addPotionEffect(new PotionEffect(PotionEffectType.SLOW,delay-2,5));
        new DelayAOEAttack(loc,monster,delay,radius,damage,new ParticleBuilder(Particle.REDSTONE).count(0).color(Color.GRAY),new PlaneParticleBuilder(Particle.SMOKE_NORMAL).radius(1).density(20),new PlaneParticleBuilder(Particle.EXPLOSION_NORMAL).radius(radius).density(25*radius*radius)).run();
        new Light(loc,delay/20+1,12).create();
        loc.getWorld().playSound(loc,Sound.ENTITY_ENDERDRAGON_AMBIENT,1,1);
        new BukkitRunnable(){
            @Override
            public void run() {
                Location loc2 =loc.clone();
                loc2.setY(loc.getY()+3);
                monster.teleport(loc2);
            }
        }.runTaskLater(Main.getPlugin(Main.class),delay-10);
        new BukkitRunnable(){
            @Override
            public void run() {
                loc.getWorld().playSound(loc,Sound.ENTITY_ENDERDRAGON_FIREBALL_EXPLODE,1,1);
            }
        }.runTaskLater(Main.getPlugin(Main.class),delay);
        return true;
    }
}
