package net.acnull.arpg.monster.aiitem;

import com.zeus.pathfinder.AI.AIItem.AbstractAIItem;
import org.bukkit.Location;
import org.bukkit.entity.Monster;

public class TeleportAIItem extends AbstractAIItem {

    private final Monster monster;
    private final int maxRange;

    public TeleportAIItem(Monster monster,int maxRange){
        this.monster=monster;
        this.maxRange=maxRange;
    }

    @Override
    public boolean shouldExecute() {
        if(monster.getTarget()==null){
            return false;
        }
        if(monster.getTarget().isDead()){
            return false;
        }
        Location loc=monster.getLocation();
        Location loc1=monster.getTarget().getLocation();
        if(loc.getWorld()!=loc1.getWorld()){
            return false;
        }
        return ((loc.getX()-loc1.getX())*(loc.getX()-loc1.getX())
                +(loc.getY()-loc1.getY())*(loc.getY()-loc1.getY())
                +(loc.getZ()-loc1.getZ())*(loc.getZ()-loc1.getZ()))>maxRange*maxRange;
    }

    @Override
    public void startExecute() {
        monster.getTarget().teleport(monster.getLocation());
    }

    @Override
    public boolean continueExecute() {
        return false;
    }
}
