package net.acnull.arpg.monster;

import com.zeus.pathfinder.AI.AIItem.AbstractAIItem;
import com.zeus.pathfinder.Manager.AIManager;
import net.acnull.arpg.Main;
import net.acnull.arpg.monster.aiitem.AbstractCooldownAIItem;
import org.bukkit.entity.LivingEntity;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class CooldownAIManager extends AIManager {
    private List<AbstractCooldownAIItem> ailist;
    private int interval;

    public CooldownAIManager(LivingEntity le,int _interval) {
        super(le);
        ailist=new ArrayList<>();
        interval=_interval;
    }

    @Override
    public void addAI(AbstractAIItem ai, int prop){
        super.addAI(ai,prop);
        if(ai instanceof AbstractCooldownAIItem) ailist.add( (AbstractCooldownAIItem) ai);
    }

    @Override
    public void setAI(List<AbstractAIItem> list) throws IllegalAccessException, NoSuchFieldException, ClassNotFoundException {
        super.setAI(list);
        for(AbstractAIItem ai:list){
            if(ai instanceof AbstractCooldownAIItem) ailist.add((AbstractCooldownAIItem) ai);
        }
    }

    @Override
    public void killAI() throws IllegalAccessException, NoSuchFieldException, ClassNotFoundException {
        super.killAI();
        ailist.clear();
    }

    public void focus(AbstractCooldownAIItem _ai,int delay){
        for(AbstractCooldownAIItem ai:ailist){
            if(!ai.equals(_ai)) ai.suspend(true);
        }
        new BukkitRunnable(){
            @Override
            public void run() {
                for(AbstractCooldownAIItem ai:ailist){
                    ai.suspend(false);
                }
            }
        }.runTaskLater(Main.getPlugin(Main.class),interval+delay);
    }

    public AbstractCooldownAIItem fromUUID(UUID uuid){
        for(AbstractCooldownAIItem ai : ailist){
            if(ai.getUuid().equals(uuid)) return ai;
        }
        return null;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }
}
