package net.acnull.arpg.monster.monsters;

import com.zeus.pathfinder.AI.AIItem.AbstractAIItem;
import net.acnull.arpg.monster.CooldownAIManager;
import net.acnull.arpg.monster.IMonster;
import net.acnull.arpg.monster.aiitem.AOEAttackAIItem;
import net.acnull.arpg.monster.aiitem.FlashChopAttackAIItem;
import org.bukkit.entity.Monster;

public class Xinchang implements IMonster {

    @Override
    public String getDisplayName() {
        return "§f§l[§e§l限定§f§l]§c§lboss§f§l织田信长";
    }

    @Override
    public AbstractAIItem[] getAIItems(Monster monster) {
        CooldownAIManager manager=new CooldownAIManager(monster,20);
        return new AbstractAIItem[]{
            new AOEAttackAIItem(monster,40,5,18,8 ,14*20,manager),
            new FlashChopAttackAIItem(monster,20,5,8,8*20,manager)
        };
    }

    @Override
    public boolean isNoSprint() {
        return true;
    }
}
