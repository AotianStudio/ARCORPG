package net.acnull.arpg.monster.monsters;

import com.destroystokyo.paper.ParticleBuilder;
import com.zeus.pathfinder.AI.AIItem.AbstractAIItem;
import net.acnull.arpg.monster.IMonster;
import net.acnull.arpg.monster.aiitem.LineAttackAIItem;
import net.acnull.arpg.monster.aiitem.SFAttackAIItem;
import net.acnull.arpg.monster.aiitem.TeleportAIItem;
import net.acnull.arpg.particle.FiveFlowerParticleBuilder;
import net.acnull.arpg.particle.RedStoneParticleBuilder;
import org.bukkit.Particle;
import org.bukkit.entity.Monster;

public class Jiakang implements IMonster {
    @Override
    public String getDisplayName() {
        return "§c§lboss§f§l德川家康";
    }

    @Override
    public AbstractAIItem[] getAIItems(Monster monster) {
        return new AbstractAIItem[]{
                new LineAttackAIItem(monster, new ParticleBuilder(Particle.FLAME).count(0),14,30,12,2),
                new TeleportAIItem(monster,25),
                new SFAttackAIItem(monster,1,15,10,50,10
                        ,new RedStoneParticleBuilder().radius(0.05f).color(255,204,255).count(1)
                        ,new FiveFlowerParticleBuilder().radius(1f).color(255,204,255).count(5)),
        };
    }

    @Override
    public boolean isNoSprint() {
        return true;
    }
}
